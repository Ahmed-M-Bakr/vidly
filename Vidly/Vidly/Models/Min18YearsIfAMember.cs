﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Min18YearsIfAMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (Customer) validationContext.ObjectInstance;

            if (customer.MemberShipTypeId == MemberShipType.Unknown 
                || customer.MemberShipTypeId == MemberShipType.PayAsYouGo)
            {
                return ValidationResult.Success;
            }
            if(customer.BirthDate == null)
                return new ValidationResult("Birthdate is required");

            if(DateTime.Now.Year - customer.BirthDate.Value.Year >= 18)
                return ValidationResult.Success;
            return new ValidationResult("You must be greater than 18");
        }
    }
}