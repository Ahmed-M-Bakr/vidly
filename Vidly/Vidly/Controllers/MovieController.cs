﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity;

namespace Vidly.Controllers
{
    public class MovieController : Controller
    {
        private ApplicationDbContext _context;

        public MovieController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Movie/Random
        public ActionResult Random()
        {
            var movie = new Movie() {Name = "Shrek"};

            var customers = new List<Customer>
            {
                new Customer { Name = "Customer 1"},
                new Customer {Name = "Customer 2"}
            };

            var viewModel = new RandomeMovieViewModel
            {
                Movie = movie,
                Customers = customers
            };
            return View(viewModel);
        }

        [Authorize(Roles = RoleName.CanManageMovies)]//you can add more than one attribute name using comma
        //you must call something like http://localhost:59691/movie/edit/1
        public ActionResult Edit(int id)
        {
            var newMovieModelView = new NewMovieViewModel
            {
                Movie = _context.Movies.First(m=>m.Id == id),
                Genres = _context.Genres.ToList()
            };
            return View("New", newMovieModelView);
        }

        public ActionResult Details(int id)
        {
            Movie movie = _context.Movies.Include(m=> m.Genre).FirstOrDefault(m => m.Id == id);

            if (movie == null)
                return HttpNotFound();
            return View(movie);
        }

        //you must call something like http://localhost:59691/movie
        public ActionResult Index()
        {
            var movies = _context.Movies.Include(m => m.Genre).ToList();
            var moviesViewModel = new MoviesViewModel { Movies = movies};
            if (User.IsInRole(RoleName.CanManageMovies))
                return View(moviesViewModel);
            else
                return View("ReadOnlyIndex", moviesViewModel);
        }

        //you must call something like http://localhost:59691/movie/released/2015/04
        [Route("movie/released/{year:int}/{month:regex(\\d{2}):range(1,12)}")]
        //add 1 constrain to the year which is the integer type.
        //add 2 constrains to the month attribute which are the regex and the range.
        //note that the {year} in the above route must be the same name as the parameter passed
        //to the function as shown in the function below, the same for the month for sure.
        //We can add more constrains like: min, max, length, minlength, maxlength, int, float, double
        //datetime, bool, guid.
        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(year + "/" + month);
        }

        public ActionResult New()
        {
            var newMovieViewModel = new NewMovieViewModel
            {
                Movie = new Movie(),
                Genres = _context.Genres.ToList()
            };
            return View(newMovieViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                var modelView = new NewMovieViewModel
                {
                    Movie = movie,
                    Genres = _context.Genres.ToList()
                };
                return View("New", modelView);
            }
            if (movie.Id == 0)
            {
                _context.Movies.Add(movie);
            }
            else
            {
                var movieFromDb = _context.Movies.First();
                movieFromDb.Name = movie.Name;
                movieFromDb.AddedDate = movie.AddedDate;
                movieFromDb.NumberInStock = movie.NumberInStock;
                movieFromDb.GenreId = movie.GenreId;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Movie");
        }
    }
}