﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomerController : Controller
    {
        private ApplicationDbContext _context;

        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Customer
        public ActionResult Index()
        {
            var customers = _context.Customers.Include(c => c.MemberShipType).ToList();//you must add using System.Data.Entity;
            //the include method is called so that it includes data from the referenced table [MemberShipType]

            CustomersViewModel customersViewModel = new CustomersViewModel {Customers = customers};
            return View(customersViewModel);
        }

        public ActionResult Details(int id)
        {
            bool idExists = false;
            var customer = _context.Customers.Include(c=>c.MemberShipType).SingleOrDefault(c => c.Id == id);
            if (customer == null)
                return HttpNotFound();
            return View(customer);
        }

        public ActionResult New()
        {
            var membershipTypes = _context.MemberShipTypes.ToList();
            var newCustomerViewModel = new NewCustomerViewModel
            {
                Customer = new Customer(),//to get rid of the message: id field is required when using validation summary
                MemberShipTypes = membershipTypes
            };
            return View(newCustomerViewModel);
        }

        [HttpPost] //to make sure it uses the post not git method
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)//We define a function parameter so that it automatically bind this object to the submit request 
        {
            //if you want to validate the validation data on each parameter in customer
            //in customer class name is required and the max length is 255, and so on
            if (!ModelState.IsValid)
            {
                var viewModel = new NewCustomerViewModel
                {
                    Customer = customer,
                    MemberShipTypes = _context.MemberShipTypes.ToList()
                };
                return View("New", viewModel);
            }
            if (customer.Id == 0)
            {
                _context.Customers.Add(customer); //Add new customer in the memory not in DB
            }
            else
            {
                var customerInDb = _context.Customers.First(c => c.Id == customer.Id);
                customerInDb.Name = customer.Name;
                customerInDb.IsSubscripedToNewsLetter = customer.IsSubscripedToNewsLetter;
                customerInDb.MemberShipTypeId = customer.MemberShipTypeId;
            }
            _context.SaveChanges(); //Apply all changes to the DB

            return RedirectToAction("Index", "Customer");
        }

        public ActionResult Edit(int id)
        {
            var newCustomerViewModel = new NewCustomerViewModel
            {
                Customer = _context.Customers.FirstOrDefault(c => c.Id == id),
                MemberShipTypes = _context.MemberShipTypes.ToList()
            };
            return View("New", newCustomerViewModel);//We can not just type return View(), as it expects a html file with the name edit, so we pass the name of the HTML to view and pass the parameters to it
        }
    }
}