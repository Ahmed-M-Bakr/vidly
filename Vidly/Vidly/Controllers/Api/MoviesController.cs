﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class MoviesController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        //GET: /api/movies
        public IEnumerable<MovieDto> GetMovies()
        {
            return _context.Movies.ToList().Select(Mapper.Map<Movie, MovieDto>);
        }

        //GET: /api/movies/1
        public IHttpActionResult GetMovie(int id)
        {
            var movieFromDb = _context.Movies.FirstOrDefault(m => m.Id == id);
            if (movieFromDb == null)
                return BadRequest();

            var movieDto = Mapper.Map<Movie, MovieDto>(movieFromDb);
            return Ok(movieDto);
        }


        //POST: /api/movies
        [HttpPost]
        public IHttpActionResult CreateMovie(MovieDto movieDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (movieDto == null)
                return BadRequest();
            var movie = Mapper.Map<MovieDto, Movie>(movieDto);
            _context.Movies.Add(movie);
            _context.SaveChanges();

            movieDto.Id = movie.Id;
            var uri = new Uri(Request.RequestUri + "/" + movie.Id);
            return Created(uri, movieDto);
        }

        //PUT: /api/movies/1
        [HttpPut]
        public IHttpActionResult UpdateMovie(int id, MovieDto movieDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var movieFromDb = _context.Movies.FirstOrDefault(m => m.Id == id);
            if (movieFromDb == null)
                return BadRequest();
            Mapper.Map<MovieDto, Movie>(movieDto, movieFromDb);
            _context.SaveChanges();
            return Ok(movieDto);
        }

        //DELETE: /api/movies/1
        [HttpDelete]
        public IHttpActionResult DeleteMovie(int id)
        {
            var movie = _context.Movies.FirstOrDefault(m => m.Id == id);
            if (movie == null)
                return BadRequest();
            _context.Movies.Remove(movie);
            _context.SaveChanges();
            return Ok();
        }
    }
}
