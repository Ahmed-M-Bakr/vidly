﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        //GET /api/customers
        public IEnumerable<CustomerDto> GetCustomers()
        {
            return _context.Customers.ToList().Select(Mapper.Map<Customer, CustomerDto>);//pass a deligate which is a pointer to the function map
        }

        //GET /api/customers/1
        public IHttpActionResult GetCustomer(int id)
        {
            Customer customer = _context.Customers.FirstOrDefault(c => c.Id == id);
            if (customer == null)
                return BadRequest();
            return Ok(Mapper.Map<Customer, CustomerDto>(customer));
        }

        //POST /api/customers
        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            _context.Customers.Add(customer);
            _context.SaveChanges();

            customerDto.Id = customer.Id;
            var uri = new Uri(Request.RequestUri + "/" + customer.Id);//api/customers/{Id}
            return Created(uri, customerDto);
        }

        //PUT /api/customers/1
        [HttpPut]
        public IHttpActionResult UpdateCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var customerFromDb = _context.Customers.FirstOrDefault(c => c.Id == customerDto.Id);
            if (customerFromDb == null)
                return BadRequest();
            Mapper.Map(customerDto, customerFromDb);

            _context.SaveChanges();
            return Ok(customerDto);
        }

        //DELETE /api/customers/1
        [HttpDelete]
        public IHttpActionResult DeleteCustomer(int id)
        {
            var customerFromDb = _context.Customers.FirstOrDefault(c => c.Id == id);
            if (customerFromDb == null)
                return BadRequest();

            _context.Customers.Remove(customerFromDb);
            _context.SaveChanges();
            return Ok();
        }
    }
}
