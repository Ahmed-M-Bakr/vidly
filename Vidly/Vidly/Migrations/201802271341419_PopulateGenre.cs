namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGenre : DbMigration
    {
        public override void Up()
        {
            Sql("insert into Genres (id, name) values (1, 'Comedy')");
            Sql("insert into Genres (id, name) values (2, 'Crime')");
            Sql("insert into Genres (id, name) values (3, 'Horor')");
            Sql("insert into Genres (id, name) values (4, 'Action')");
            Sql("insert into Genres (id, name) values (5, 'Drama')");
            Sql("insert into Genres (id, name) values (6, 'Animation')");
            Sql("insert into Genres (id, name) values (7, 'Science Fiction')");
        }
        
        public override void Down()
        {
        }
    }
}
