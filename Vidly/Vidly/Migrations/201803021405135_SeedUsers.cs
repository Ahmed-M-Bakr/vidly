namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6aff8dce-8efe-407c-b8e2-5161365566e1', N'admin@vidly.com', 0, N'AN7Q58Lz2lwfaLysKQb/lwMFwZ/uZwmrx1NuxpaArCoCGMxihaqkj3tmnod+8Hoe/g==', N'98415f80-2211-41ba-8e20-daa9178334f2', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c1a4d546-a4b5-4867-877e-845bfb076974', N'user@vidly.com', 0, N'AHPKW62/nwy85LHCYcVuZdDBzAGt2cRRDq2i/Rkn8FHabt2O/PE3u5L9r6JSIT1zrQ==', N'd0045979-f473-4139-8812-03563a1b975e', NULL, 0, 0, NULL, 1, 0, N'user@vidly.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'04578ea9-e9a5-4f31-a037-38e4d4069815', N'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6aff8dce-8efe-407c-b8e2-5161365566e1', N'04578ea9-e9a5-4f31-a037-38e4d4069815')
");
        }
        
        public override void Down()
        {
        }
    }
}
