// <auto-generated />
namespace Vidly.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class applyAnnotationOnCustomerName : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(applyAnnotationOnCustomerName));
        
        string IMigrationMetadata.Id
        {
            get { return "201802261151372_applyAnnotationOnCustomerName"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
